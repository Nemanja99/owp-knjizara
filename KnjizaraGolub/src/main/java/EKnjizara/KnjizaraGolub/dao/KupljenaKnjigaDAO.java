package EKnjizara.KnjizaraGolub.dao;

import java.util.List;

import EKnjizara.KnjizaraGolub.model.KupljenaKnjiga;

public interface KupljenaKnjigaDAO {
	
	public KupljenaKnjiga findOne(Long id);

	public List<KupljenaKnjiga> findAll();

	public int save(KupljenaKnjiga kupovina);

	public int update(KupljenaKnjiga kupovina);

	public int delete(Long id);

}
