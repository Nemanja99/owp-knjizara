package EKnjizara.KnjizaraGolub.dao;

import java.util.List;

import EKnjizara.KnjizaraGolub.model.LoyaltyKartica;



public interface LoyaltyKarticaDAO {

	public LoyaltyKartica findOne(Long id);

	public List<LoyaltyKartica> findAll();

	public int save(LoyaltyKartica loyaltyKartica);

	public int update(LoyaltyKartica loyaltyKartica);

	public int delete(Long id);
	
}
