package EKnjizara.KnjizaraGolub.dao;

import java.util.List;

import EKnjizara.KnjizaraGolub.model.Korisnik;

public interface KorisnikDAO {
	
	public Korisnik findOne(String username);

	public Korisnik findOne(String username, String password);

	public List<Korisnik> findAll();

	public List<Korisnik> find(String username, String eMail, String ime, String prezime, String datumRodjenja, String adresa, String brojTelefona, Boolean administrator);
	
	public void save(Korisnik korisnik);

	public void update(Korisnik korisnik);

	public void delete(String username);

}
