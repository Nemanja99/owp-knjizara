package EKnjizara.KnjizaraGolub.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import EKnjizara.KnjizaraGolub.dao.KorisnikDAO;
import EKnjizara.KnjizaraGolub.model.Korisnik;



@Repository
public class KorisnikDAOImpl implements KorisnikDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KorisnikRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String username = rs.getString(index++);
			String eMail = rs.getString(index++);
			String ime = rs.getString(index++);
			String prezime = rs.getString(index++);
			String datumRodjenja = rs.getString(index++);
			String adresa = rs.getString(index++);
			String brojTelefona = rs.getString(index++);
			Boolean administrator = rs.getBoolean(index++);

			Korisnik korisnik = new Korisnik(username, null, eMail, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator);
			return korisnik;
		}

	}
	@Override
	public Korisnik findOne(String username) {
		try {
			String sql = "SELECT username, eMail, ime,prezime, datumRodjenja, adresa, brojTelefona, administrator FROM korisnici WHERE username = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), username);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}
	@Override
	public Korisnik findOne(String username, String password) {
		try {
			String sql = "SELECT username, eMail, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator FROM korisnici WHERE username = ? AND password = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), username, password);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}
	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT username, eMail, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator FROM korisnici";
		return jdbcTemplate.query(sql, new KorisnikRowMapper());
	}
	@Override
	public List<Korisnik> find(String username, String eMail, String ime, String prezime, String datumRodjenja, String adresa, String brojTelefona, Boolean administrator) {
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT username, eMail, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator FROM korisnici ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(username!=null) {
			username = "%" + username + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("username LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(username);
		}
		
		if(eMail!=null) {
			eMail = "%" + eMail + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("eMail LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(eMail);
		}
		
		if(ime!=null) {
			ime = "%" + ime + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("ime LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(ime);
		}
		
		if(prezime!=null) {
			prezime = "%" + prezime + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("prezime LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(prezime);
		}
		
		if(datumRodjenja!=null) {
			datumRodjenja = "%" + datumRodjenja + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("datumRodjenja LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(datumRodjenja);
		}
		
		if(adresa!=null) {
			adresa = "%" + adresa + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("adresa LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(adresa);
		}
		
		if(brojTelefona!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("brojTelefona LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(brojTelefona);
		}
		
		if(administrator!=null) {	

			String administratorSql = (administrator)? "administrator = 1": "administrator >= 0";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(administratorSql);
			imaArgumenata = true;
		}
		
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY username";
		else
			sql=sql + " ORDER BY username";
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, listaArgumenata.toArray(), new KorisnikRowMapper());
	}
	@Override
	public void save(Korisnik korisnik) {
		String sql = "INSERT INTO korisnici (username, password, eMail, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, korisnik.getUsername(), korisnik.getPassword(), korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.isAdministrator());
	}
	@Override
	public void update(Korisnik korisnik) {
		if (korisnik.getPassword() == null) {
			String sql = "UPDATE korisnici SET eMail = ?, ime = ?, prezime = ?, datumRodjenja = ?, adresa = ?, brojTelefona = ?, administrator = ? WHERE username = ?";
			jdbcTemplate.update(sql, korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.isAdministrator(), korisnik.getUsername());
		} else {
			String sql = "UPDATE korisnici SET password = ?, eMail = ?, ime = ?, prezime = ?, datumRodjenja = ?, adresa = ?, brojTelefona = ?, administrator = ? WHERE username = ?";
			jdbcTemplate.update(sql, korisnik.getPassword(), korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.isAdministrator(), korisnik.getUsername());
		}
	}
	@Override
	public void delete(String username) {
		String sql = "DELETE FROM korisnici WHERE username = ?";
		jdbcTemplate.update(sql, username);
	}
	
}