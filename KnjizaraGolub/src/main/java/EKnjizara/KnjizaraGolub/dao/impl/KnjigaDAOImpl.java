package EKnjizara.KnjizaraGolub.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import EKnjizara.KnjizaraGolub.dao.KnjigaDAO;
import EKnjizara.KnjizaraGolub.model.Knjiga;

@Repository
public class KnjigaDAOImpl implements KnjigaDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/*@Autowired
	private ZanrDAO zanrDAO; */

	private class KnjigaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Knjiga> knjige = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long knjigaId = resultSet.getLong(index++);
			String knjigaNaziv = resultSet.getString(index++);
			Integer knjigaIsbn = resultSet.getInt(index++);
			String knjigaIzdavackaKuca = resultSet.getString(index++);
			String knjigaAutor = resultSet.getString(index++);
			Integer knjigaGodinaIzdanja = resultSet.getInt(index++);
			String knjigaOpis = resultSet.getString(index++);
			Double knjigaCena = resultSet.getDouble(index++);
			Integer knjigaBrStranica = resultSet.getInt(index++);
			String knjigaTipPoveza = resultSet.getString(index++);
			String knjigaPismo = resultSet.getString(index++);
			String knjigaJezik = resultSet.getString(index++);
			
			Knjiga knjiga = knjige.get(knjigaId);
			if (knjiga == null) {
				knjiga = new Knjiga(knjigaId, knjigaNaziv, knjigaIsbn, knjigaIzdavackaKuca, knjigaAutor, knjigaGodinaIzdanja, knjigaOpis, knjigaCena, knjigaBrStranica, knjigaTipPoveza, knjigaPismo, knjigaJezik);
				knjige.put(knjiga.getId(), knjiga); 
			}
			
		}

		public List<Knjiga> getKnjige() {
			return new ArrayList<>(knjige.values());
		}

	}
	
	@Override
	public Knjiga findOne(Long id) {
		String sql = 
				"SELECT id, naziv, isbn, izdavackaKuca, autor, godinaIzdanja, opis, cena, brStranica, tipPoveza, pismo, jezik FROM knjige WHERE id = ? ORDER BY id";

		KnjigaRowCallBackHandler rowCallbackHandler = new KnjigaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getKnjige().get(0);
	}
	
	@Override
	public List<Knjiga> findAll() {
		String sql = 
				"SELECT id, naziv, isbn, izdavackaKuca, autor, godinaIzdanja, opis, cena, brStranica, tipPoveza, pismo, jezik FROM knjige ORDER BY id";

		KnjigaRowCallBackHandler rowCallbackHandler = new KnjigaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getKnjige();
	}

	@Transactional
	@Override
	public int save(final Knjiga knjiga) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO knjige (id, naziv, isbn, izdavackaKuca, autor, godinaIzdanja, opis, cena, brStranica, tipPoveza, pismo, jezik) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setLong(index++, knjiga.getId());
				preparedStatement.setString(index++, knjiga.getNaziv());
				preparedStatement.setInt(index++, knjiga.getISBN());
				preparedStatement.setString(index++, knjiga.getIzdavackaKuca());
				preparedStatement.setString(index++, knjiga.getAutor());
				preparedStatement.setInt(index++, knjiga.getGodinaIzdanja());
				preparedStatement.setString(index++, knjiga.getOpis());
				preparedStatement.setDouble(index++, knjiga.getCena());
				preparedStatement.setInt(index++, knjiga.getBrStranica());
				preparedStatement.setString(index++, knjiga.getTipPoveza());
				preparedStatement.setString(index++, knjiga.getPismo());
				preparedStatement.setString(index++, knjiga.getJezik());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		/*if (uspeh) {
			String sql = "INSERT INTO filmZanr (filmId, zanrId) VALUES (?, ?)";
			for (Zanr itZanr: film.getZanrovi()) {	
				uspeh = uspeh && jdbcTemplate.update(sql, keyHolder.getKey(), itZanr.getId()) == 1;
			}
		}*/
		return uspeh?1:0;
	}

	@Transactional
	@Override
	public int update(Knjiga knjiga) {
		
		/*String sql = "DELETE FROM filmZanr WHERE filmId = ?";
		jdbcTemplate.update(sql, film.getId());*/
	
		//boolean uspeh = true;
		/*String sql = "INSERT INTO filmZanr (filmId, zanrId) VALUES (?, ?)";
		for (Zanr itZanr: film.getZanrovi()) {	
			uspeh = uspeh &&  jdbcTemplate.update(sql, film.getId(), itZanr.getId()) == 1;
		}*/

		String sql = "UPDATE knjige SET naziv = ?, isbn = ?, izdavackaKuca = ?, autor = ?, godinaIzdanja = ?, opis = ?, cena = ?, brStranica = ?, tipPoveza = ?, pismo = ?, jezik = ? WHERE id = ?";	
		/*uspeh = uspeh &&  */return jdbcTemplate.update(sql, knjiga.getNaziv(), knjiga.getISBN(), knjiga.getIzdavackaKuca(), knjiga.getAutor(), knjiga.getGodinaIzdanja(), knjiga.getOpis(), knjiga.getCena(), knjiga.getBrStranica(), knjiga.getTipPoveza(), knjiga.getPismo(), knjiga.getJezik(), knjiga.getId());
		
		//return uspeh?1:0;
	}

	@Transactional
	@Override
	public int delete(Long id) {
		/*String sql = "DELETE FROM filmZanr WHERE filmId = ?";
		jdbcTemplate.update(sql, id);*/

		String sql = "DELETE FROM knjige WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

	private class KnjigaRowMapper implements RowMapper<Knjiga> {

		@Override
		public Knjiga mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long knjigaId = rs.getLong(index++);
			String knjigaNaziv = rs.getString(index++);
			Integer knjigaIsbn = rs.getInt(index++);
			String knjigaIzdavackaKuca = rs.getString(index++);
			String knjigaAutor = rs.getString(index++);
			Integer knjigaGodinaIzdanja = rs.getInt(index++);
			String knjigaOpis = rs.getString(index++);
			Double knjigaCena = rs.getDouble(index++);
			Integer knjigaBrStranica = rs.getInt(index++);
			String knjigaTipPoveza = rs.getString(index++);
			String knjigaPismo = rs.getString(index++);
			String knjigaJezik = rs.getString(index++);

			Knjiga knjiga = new Knjiga(knjigaId, knjigaNaziv, knjigaIsbn, knjigaIzdavackaKuca, knjigaAutor, knjigaGodinaIzdanja, knjigaOpis, knjigaCena, knjigaBrStranica, knjigaTipPoveza, knjigaPismo, knjigaJezik);
			return knjiga;
		}

	}
	
	@Override
	public List<Knjiga> find(String naziv, Integer isbn, String izdavackaKuca, String autor, Integer godinaIzdanja, String opis, Double cena, Integer brStranica, String tipPoveza, String pismo, String jezik) {
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT k.id, k.naziv, k.isbn, k.izdavackaKuca, k.autor, k.godinaIzdanja, k.opis, k.cena, k.brStranica, k.tipPoveza, k.pismo, k.jezik FROM knjige k "; 
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(naziv!=null) {
			naziv = "%" + naziv + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.naziv LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(naziv);
		}
		
		if(isbn!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.isbn == ?");
			imaArgumenata = true;
			listaArgumenata.add(isbn);
		}

		if(izdavackaKuca!=null) {
			izdavackaKuca = "%" + izdavackaKuca + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.izdavackaKuca LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(izdavackaKuca);
		}
		
		if(autor!=null) {
			autor = "%" + autor + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.autor LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(autor);
		}
		
		if(godinaIzdanja!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.godinaIzdanja == ?");
			imaArgumenata = true;
			listaArgumenata.add(godinaIzdanja);
		}
		
		if(opis!=null) {
			opis = "%" + opis + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.opis LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(opis);
		}

		if(cena!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.cena == ?");
			imaArgumenata = true;
			listaArgumenata.add(cena);
		}
		
		if(brStranica!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.brStranica == ?");
			imaArgumenata = true;
			listaArgumenata.add(brStranica);
		}
		
		if(tipPoveza!=null) {
			tipPoveza = "%" + tipPoveza + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.tipPoveza LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(tipPoveza);
		}

		if(pismo!=null) {
			pismo = "%" + pismo + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.pismo LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(pismo);
		}
		
		if(jezik!=null) {
			jezik = "%" + jezik + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.jezik LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(jezik);
		}
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY k.id";
		else
			sql=sql + " ORDER BY k.id";
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaRowMapper());
		
		
		/****bitno!!!!!******/
		//return null; //treba da ima return pa sam morao da kliknem da samo izgenerise
		/*******/
		/*List<Knjiga> knjige = jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaRowMapper());
		for (Knjiga knjiga : knjige) {
			
			knjiga.setZanrovi(findFilmZanr(knjiga.getId(), null));
		}*/
		//ako se treži film sa određenim žanrom  
		// tada se taj žanr mora nalaziti u listi žanrova od filma
		/*if(zanrId!=null)
			for (Iterator iterator = filmovi.iterator(); iterator.hasNext();) {
				Film film = (Film) iterator.next();
				boolean zaBrisanje = true;
				for (Zanr zanr : film.getZanrovi()) {
					if(zanr.getId() == zanrId) {
						zaBrisanje = false;
						break;
					}
				}
				if(zaBrisanje)
					iterator.remove();
			}*/
		
		//return knjige;
	


	
	/*private class FilmZanrRowMapper implements RowMapper<Long []> {

		@Override
		public Long [] mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long filmId = rs.getLong(index++);
			Long zanrId = rs.getLong(index++);

			Long [] filmZanr = {filmId, zanrId};
			return filmZanr;
		}
	}*/
	
	/*private List<Zanr> findFilmZanr(Long filmId, Long zanrId) {
		
		List<Zanr> znaroviFilma = new ArrayList<Zanr>();
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = 
				"SELECT fz.filmId, fz.zanrId FROM filmZanr fz ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(filmId!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("fz.filmId = ?");
			imaArgumenata = true;
			listaArgumenata.add(filmId);
		}
		
		if(zanrId!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("fz.zanrId = ?");
			imaArgumenata = true;
			listaArgumenata.add(zanrId);
		}

		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY fz.filmId";
		else
			sql=sql + " ORDER BY fz.filmId";
		System.out.println(sql);
		
		List<Long[]> filmZanrovi = jdbcTemplate.query(sql, listaArgumenata.toArray(), new FilmZanrRowMapper()); 
				
		for (Long[] fz : filmZanrovi) {
			znaroviFilma.add(zanrDAO.findOne(fz[1]));
		}
		return znaroviFilma;
	}*/
	}
}
