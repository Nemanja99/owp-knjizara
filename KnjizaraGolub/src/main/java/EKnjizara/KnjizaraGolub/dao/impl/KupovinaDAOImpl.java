package EKnjizara.KnjizaraGolub.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import EKnjizara.KnjizaraGolub.dao.KnjigaDAO;
import EKnjizara.KnjizaraGolub.dao.KorisnikDAO;
import EKnjizara.KnjizaraGolub.dao.KupovinaDAO;
import EKnjizara.KnjizaraGolub.model.Knjiga;
import EKnjizara.KnjizaraGolub.model.Korisnik;
import EKnjizara.KnjizaraGolub.model.Kupovina;

@Repository
public class KupovinaDAOImpl implements KupovinaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KnjigaDAO knjigaDAO;
	
	@Autowired
	private KorisnikDAO korisnikDAO;

	private class KupovinaRowMapper implements RowMapper<Kupovina> {

		@Override
		public Kupovina mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int kupovinaId = rs.getInt(index++);
			
			//Long knjigaId = rs.getLong(index++);
				
			//Knjiga knjiga = knjigaDAO.findOne(knjigaId);

			String datumKupovine = rs.getString(index++);
			
			String username = rs.getString(index++);

			Korisnik korisnik = korisnikDAO.findOne(username);
			
			double ukupnaCena = rs.getDouble(index++);
			
			String ukupanBrKupljenihKnjiga = rs.getString(index++);
			
			Kupovina kupovina = new Kupovina(kupovinaId, datumKupovine , korisnik, ukupnaCena, ukupanBrKupljenihKnjiga);
			return kupovina;
		}

	}

	@Override
	public Kupovina findOne(Long id) {
		String sql = 
				"SELECT k.id, k.datumKupovine, k.ukupnaCena, k.ukupanBrKupljenihKnjiga , u.username FROM kupovina k "
				+"LEFT JOIN korisnici u on u.username = k.korisnik " 
				+"WHERE k.id = ? " 
				+"ORDER BY k.id";
		return jdbcTemplate.queryForObject(sql, new KupovinaRowMapper(), id);
	}

	@Override
	public List<Kupovina> findAll() {
		String sql = 
				"SELECT k.id,k.datumKupovine, k.ukupnaCena, k.ukupanBrKupljenihKnjiga, u.username FROM kupovina k " 
				+"LEFT JOIN korisnici u on u.username = k.korisnik " 
				+"ORDER BY k.id";
		return jdbcTemplate.query(sql, new KupovinaRowMapper());
	}

	@Override
	public int save(Kupovina kupovina) {
		System.out.println(kupovina.getUkupnaCena());
		String sql = "INSERT INTO kupovina (datumKupovine, korisnik, ukupnaCena, ukupanBrKupljenihKnjiga) VALUES (?, ? ,?, ?)";
		return jdbcTemplate.update(sql, kupovina.getDatumKupovine(), kupovina.getKorisnik().getUsername(), kupovina.getUkupnaCena() ,  kupovina.getUkupanBrKupljenihKnjiga());
	}

	@Override
	public int update(Kupovina kupovina) {
		String sql = "UPDATE kupovina SET datumKupovine = ?,korisnik = ?, ukupnaCena = ?,  ukupanBrKupljenihKnjiga = ? WHERE id = ?";
		return jdbcTemplate.update(sql, kupovina.getDatumKupovine(), kupovina.getUkupnaCena() , kupovina.getKorisnik().getUsername(), kupovina.getUkupanBrKupljenihKnjiga(), kupovina.getId());
	}
	
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM kupovina WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

}
