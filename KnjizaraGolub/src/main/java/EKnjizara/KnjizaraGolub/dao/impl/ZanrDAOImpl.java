package EKnjizara.KnjizaraGolub.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import EKnjizara.KnjizaraGolub.dao.ZanrDAO;
import EKnjizara.KnjizaraGolub.model.Zanr;



@Repository
@Primary
public class ZanrDAOImpl implements ZanrDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class ZanrRowMapper implements RowMapper<Zanr> {

		@Override
		public Zanr mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String ime = rs.getString(index++);
			String opis = rs.getString(index++);

			Zanr zanr = new Zanr(id, ime, opis);
			return zanr;
		}

	}
	@Override
	public Zanr findOne(Long id) {
		String sql = "SELECT id, ime, opis FROM zanrovi WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new ZanrRowMapper(), id);
	}
	@Override
	public List<Zanr> findAll() {
		String sql = "SELECT id, ime, opis FROM zanrovi";
		return jdbcTemplate.query(sql, new ZanrRowMapper());
	}
	@Override
	public List<Zanr> find(String ime, String opis) {
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT z.id , z.ime, z.opis FROM zanrovi z "; 
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(ime!=null) {
			ime = "%" + ime + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("z.ime LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(ime);
		}
		
		if(opis!=null) {
			opis = "%" + opis + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("z.opis LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(opis);
		}
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY z.id";
		else
			sql=sql + " ORDER BY z.id";
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, listaArgumenata.toArray(), new ZanrRowMapper());
	}
	@Override
	public int save(Zanr zanr) {
		String sql = "INSERT INTO zanrovi (id, ime , opis) VALUES (?, ?, ?)";
		return jdbcTemplate.update(sql,zanr.getId(), zanr.getIme(), zanr.getOpis());
	}
	
	@Override
	public int update(Zanr zanr) {
		String sql = "UPDATE zanrovi SET ime = ?, opis = ? WHERE id = ?";
		return jdbcTemplate.update(sql, zanr.getIme(), zanr.getOpis() , zanr.getId());
	}
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM zanrovi WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}
	@Override
	public int[] save(ArrayList<Zanr> zanrovi) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
