package EKnjizara.KnjizaraGolub.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import EKnjizara.KnjizaraGolub.dao.KnjigaDAO;
import EKnjizara.KnjizaraGolub.dao.KupljenaKnjigaDAO;
import EKnjizara.KnjizaraGolub.model.Knjiga;
import EKnjizara.KnjizaraGolub.model.KupljenaKnjiga;

@Repository
public class KupljenaKnjigaDAOImpl implements KupljenaKnjigaDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KnjigaDAO knjigaDAO;
	
	/*@Autowired
	private KorisnikDAO korisnikDAO;*/

	private class KupljenaKnjigaRowMapper implements RowMapper<KupljenaKnjiga> {

		@Override
		public KupljenaKnjiga mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int kupljenaKnjigaId = rs.getInt(index++);
			
			Long knjigaId = rs.getLong(index++);
			
			Knjiga knjiga = knjigaDAO.findOne(knjigaId);
			
			String brPrimeraka = rs.getString(index++);
			
			double cena = rs.getDouble(index++);
			
			//String korisnickoIme = rs.getString(index++);

			//Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
			
			KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga(kupljenaKnjigaId, knjiga , brPrimeraka, cena);
			return kupljenaKnjiga;
		}

	}

	@Override
	public KupljenaKnjiga findOne(Long id) {
		String sql = 
				"SELECT k.id, k.brPrimeraka, k.cena, g.naziv FROM kupljenaKnjiga k "
				+"LEFT JOIN knjige g on g.naziv = k.knjiga " 
				+"WHERE k.id = ? " 
				+"ORDER BY k.id";
		return jdbcTemplate.queryForObject(sql, new KupljenaKnjigaRowMapper(), id);
	}

	@Override
	public List<KupljenaKnjiga> findAll() {
		String sql = 
				"SELECT k.id, k.brPrimeraka, k.cena, g.naziv FROM kupljenaKnjiga k " 
				+"LEFT JOIN knjige g on g.naziv = k.knjiga " 
				+"ORDER BY k.id";
		return jdbcTemplate.query(sql, new KupljenaKnjigaRowMapper());
	}

	@Override
	public int save(KupljenaKnjiga kupljenaKnjiga) {
		
		String sql = "INSERT INTO kupljenaKnjiga (knjiga, brPrimeraka, cena) VALUES (?, ? ,?)";
		return jdbcTemplate.update(sql, kupljenaKnjiga.getKnjiga().getNaziv(), kupljenaKnjiga.getBrPrimeraka() , kupljenaKnjiga.getCena());
	}

	@Override
	public int update(KupljenaKnjiga kupljenaKnjiga) {
		String sql = "UPDATE kupljenaKnjiga SET knjiga = ?,brPrimeraka = ?, cena = ? WHERE id = ?";
		return jdbcTemplate.update(sql, kupljenaKnjiga.getKnjiga().getNaziv(), kupljenaKnjiga.getBrPrimeraka() , kupljenaKnjiga.getCena(),  kupljenaKnjiga.getId());
	}
	
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM kupljenaKnjiga WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

}
