package EKnjizara.KnjizaraGolub.dao;

import java.util.List;

import EKnjizara.KnjizaraGolub.model.Knjiga;


public interface KnjigaDAO {
	
	public Knjiga findOne(Long id);

	public List<Knjiga> findAll();

	public int save(Knjiga knjiga);

	public int update(Knjiga knjiga);

	public int delete(Long id);
	
	public List<Knjiga> find(String naziv, Integer isbn, String izdavackaKuca, String autor, Integer godinaIzdanja, String opis, Double cena, Integer brStranica, String tipPoveza, String pismo, String jezik);

}
