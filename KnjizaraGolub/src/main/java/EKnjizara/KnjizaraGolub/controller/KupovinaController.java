package EKnjizara.KnjizaraGolub.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import EKnjizara.KnjizaraGolub.model.Knjiga;
import EKnjizara.KnjizaraGolub.model.Korisnik;
import EKnjizara.KnjizaraGolub.model.Kupovina;
import EKnjizara.KnjizaraGolub.service.KnjigaService;
import EKnjizara.KnjizaraGolub.service.KupovinaService;

@Controller
@RequestMapping(value="/Kupovina")
public class KupovinaController {

public static final String IZABRANE_KNJIGE_ZA_KORISNIKA = "IzabraneKnjigeZaKorisnika";
	
	private  String bURL; 
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private KupovinaService kupovinaService;
	

	@PostConstruct
	public void init() {	
	
		bURL = servletContext.getContextPath()+"/";			
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping
	public ModelAndView get(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<Kupovina> kupovina = kupovinaService.findAll();

		ModelAndView rezultat = new ModelAndView("kupovina");
		rezultat.addObject("kupovina", kupovina);

		return rezultat;
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping
	public void kupiKnjigu(
			//@RequestParam String datumKupovine,
			@RequestParam String knjigaId,
			@RequestParam String username,
			//@RequestParam double ukupnaCena,
			@RequestParam String ukupanBrKupljenihKnjiga,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		Date d = new Date();
		String datumKupovine = d.getDate() + "." + (d.getMonth() + 1 ) + "." + (d.getYear() + 1900) + ".";
		System.out.println(datumKupovine);
		
		double ukupnaCena = 0;
		
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KorisnikController.KORISNIK_KEY);
		if(korisnik==null || korisnik.isAdministrator()==true) {
			
			response.sendRedirect(bURL+"prijava.html");
			return;
		}
		
		Long idKnjige = new Long(knjigaId);
		
		if(knjigaId!=null && idKnjige<=0) {
			response.sendRedirect(bURL+"Knjige");
			return;
		}
		
		Knjiga knjiga = knjigaService.findOne(idKnjige);
		
		if(knjiga==null) {
			response.sendRedirect(bURL+"Knjige");
			return;
		}
		
		
		ukupnaCena = Integer.parseInt(ukupanBrKupljenihKnjiga) * knjiga.getCena();
		
		Kupovina kupovina = new Kupovina(datumKupovine, korisnik, ukupnaCena, ukupanBrKupljenihKnjiga);
		kupovinaService.save(kupovina);
		
		response.sendRedirect(bURL + "Kupovina"); 
		return;
	}

}
