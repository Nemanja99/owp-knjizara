package EKnjizara.KnjizaraGolub.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import EKnjizara.KnjizaraGolub.model.Korisnik;
import EKnjizara.KnjizaraGolub.service.KorisnikService;



@Controller
@RequestMapping(value="/Korisnici")
public class KorisnikController {

public static final String KORISNIK_KEY = "prijavljeniKorisnik";
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}

	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) String username,
			@RequestParam(required=false) String eMail,
			@RequestParam(required=false) String ime,
			@RequestParam(required=false) String prezime,
			@RequestParam(required=false) String datumRodjenja,
			@RequestParam(required=false) String adresa,
			@RequestParam(required=false) String brojTelefona,
			@RequestParam(required=false) Boolean administrator,
			HttpSession session, HttpServletResponse response) throws IOException {		

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL);
			return null;
		}

		
		if(username!=null && username.trim().equals(""))
			username=null;
		
		if(eMail!=null && eMail.trim().equals(""))
			eMail=null;
		
		if(ime!=null && ime.trim().equals(""))
			ime=null;
		if(prezime!=null && prezime.trim().equals(""))
			prezime=null;
		
		if(datumRodjenja!=null && datumRodjenja.trim().equals(""))
			datumRodjenja=null;
		
		if(adresa!=null && adresa.trim().equals(""))
			adresa=null;
		
		if(brojTelefona!=null && brojTelefona.equals(""))
			brojTelefona=null;
		
		List<Korisnik> korisnici = korisnikService.find(username, eMail, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator);

		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", korisnici);

		return rezultat;
	}

	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam String username, 
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		if (prijavljeniKorisnik == null || (!prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.getUsername().equals(username))) {
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}

		Korisnik korisnik = korisnikService.findOne(username);
		if (korisnik == null) {
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}

		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject("korisnik", korisnik);

		return rezultat;
	}

	@GetMapping(value="/Create")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return "knjige";
		}

		return "dodavanjeKorisnika";
	}
	
	@PostMapping(value="/Create")
	public void create(@RequestParam String username, @RequestParam String password, 
			@RequestParam String eMail, @RequestParam String ime, @RequestParam String prezime, @RequestParam String datumRodjenja, @RequestParam String adresa, @RequestParam String brojTelefona, @RequestParam(required=false) String administrator,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Korisnici");
			return;
		}

		Korisnik postojeciKorisnik = korisnikService.findOne(username);
		if (postojeciKorisnik != null) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (username.equals("") || password.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (eMail.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (ime.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (prezime.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (datumRodjenja.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (adresa.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (brojTelefona.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}

		Korisnik korisnik = new Korisnik(username, password, eMail, ime, prezime , datumRodjenja, adresa, brojTelefona, administrator != null);
		korisnikService.save(korisnik);

		response.sendRedirect(baseURL + "Korisnici");
	}

	@PostMapping(value="/Edit")
	public void edit(@RequestParam String username, 
			@RequestParam String password, String eMail, @RequestParam String ime,  @RequestParam String prezime, @RequestParam String datumRodjenja, @RequestParam String adresa, @RequestParam String brojTelefona, @RequestParam(required=false) String administrator,
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		if (prijavljeniKorisnik == null || (!prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.getUsername().equals(username))) {
			response.sendRedirect(baseURL + "Korisnici");
			return;
		}

		// validacija
		Korisnik korisnik = korisnikService.findOne(username);
		if (korisnik == null) {
			response.sendRedirect(baseURL + "Korisnici");
			return;
		}
		if (eMail.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Edit?username=" + username);
			return;
		}
		if (ime.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Edit?username=" + username);
			return;
		}
		if (prezime.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Edit?username=" + username);
			return;
		}
		if (datumRodjenja.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Edit?username=" + username);
			return;
		}
		if (adresa.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Edit?username=" + username);
			return;
		}
		if (brojTelefona.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Edit?username=" + username);
			return;
		}

		if (!password.equals("")) {
			korisnik.setPassword(password);
		}
		korisnik.setEmail(eMail);
		korisnik.setIme(ime);
		korisnik.setPrezime(prezime);
		korisnik.setDatumRodjenja(datumRodjenja);
		korisnik.setAdresa(adresa);
		korisnik.setBrojTelefona(brojTelefona);

		if (prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.equals(korisnik)) {
			korisnik.setAdministrator(administrator != null);
		}
		korisnikService.update(korisnik);

		if (!prijavljeniKorisnik.equals(korisnik)) {
			// TODO odjaviti korisnika
		}

		if (prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Korisnici");
		} else {
			response.sendRedirect(baseURL);
		}
	}

	@PostMapping(value="/Delete")
	public void delete(@RequestParam String username, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		// samo administrator može da briše korisnike, ali ne i sebe
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator() || prijavljeniKorisnik.getUsername().equals(username)) {
			response.sendRedirect(baseURL + "Korisnici");
			return;
		}

		korisnikService.delete(username);
		
		response.sendRedirect(baseURL + "Korisnici");
	}

	@PostMapping(value="/Register")
	public ModelAndView register(@RequestParam String username, @RequestParam String password, 
			@RequestParam String eMail,  @RequestParam String ime,  @RequestParam String prezime, @RequestParam String datumRodjenja, @RequestParam String adresa, @RequestParam String brojTelefona, @RequestParam String ponovljenaLozinka,
			HttpSession session, HttpServletResponse response) throws IOException {
		try {
			// validacija
			Korisnik postojeciKorisnik = korisnikService.findOne(username);
			if (postojeciKorisnik != null) {
				throw new Exception("Korisničko ime već postoji!");
			}
			if (username.equals("") || password.equals("")) {
				throw new Exception("Korisničko ime i lozinka ne smeju biti prazni!");
			}
			if (!password.equals(ponovljenaLozinka)) {
				throw new Exception("Lozinke se ne podudaraju!");
			}
			if (eMail.equals("")) {
				throw new Exception("E-mail ne sme biti prazan!");
			}
			if (ime.equals("")) {
				throw new Exception("Ime ne sme biti prazno!");
			}
			if (prezime.equals("")) {
				throw new Exception("Prezime ne sme biti prazno!");
			}
			if (datumRodjenja.equals("")) {
				throw new Exception("Datum rodjenja ne sme biti prazno!");
			}
			if (adresa.equals("")) {
				throw new Exception("Adresa ne sme biti prazna!");
			}
			if (brojTelefona.equals("")) {
				throw new Exception("Broj telefona ne sme biti prazan!");
			}

			Korisnik korisnik = new Korisnik(username, password, eMail, ime, prezime , datumRodjenja, adresa, brojTelefona);
			korisnikService.save(korisnik);

			response.sendRedirect(baseURL + "prijava.html");
			return null;
		} catch (Exception ex) {
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna registracija!";
			}

			ModelAndView rezultat = new ModelAndView("registracija");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}
	
	@PostMapping(value="/Login")
	public ModelAndView postLogin(@RequestParam String username, @RequestParam String password, 
			HttpSession session, HttpServletResponse response) throws IOException {
		try {
			Korisnik korisnik = korisnikService.findOne(username, password);
			if (korisnik == null) {
				throw new Exception("Neispravno korisničko ime ili lozinka!");
			}			

			session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
			
			response.sendRedirect(baseURL);
			return null;
		} catch (Exception ex) {
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna prijava!";
			}
			
			ModelAndView rezultat = new ModelAndView("prijava");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}

	@GetMapping(value="/Logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		
		session.invalidate();
		
		response.sendRedirect(baseURL);
	}
	
}
