package EKnjizara.KnjizaraGolub.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import EKnjizara.KnjizaraGolub.model.Korisnik;
import EKnjizara.KnjizaraGolub.model.Zanr;
import EKnjizara.KnjizaraGolub.service.ZanrService;

@Controller
@RequestMapping(value="/Zanrovi")
public class ZanroviKontroler {

	@Autowired
	private ZanrService zanrService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index(@RequestParam(required=false) String ime, @RequestParam(required=false) String opis, HttpSession session) {
		
		if(ime!=null && ime.trim().equals(""))
			ime=null;
		
		if(opis!=null && opis.trim().equals(""))
			opis=null;
		
		List<Zanr> zanrovi = zanrService.find(ime, opis);

		ModelAndView rezultat = new ModelAndView("zanrovi");
		rezultat.addObject("zanrovi", zanrovi);

		return rezultat;
	}

	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Long id, HttpSession session) throws IOException {

		Zanr zanr = zanrService.findOne(id);

		ModelAndView rezultat = new ModelAndView("zanr");
		rezultat.addObject("zanr", zanr);

		return rezultat;
	}

	@GetMapping(value="/Create")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Zanrovi");
			return "zanrovi";
		}

		return "dodavanjeZanra";
	}
	
	@PostMapping(value="/Create")
	public void create(@RequestParam Long id, @RequestParam String ime, @RequestParam String opis,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Zanrovi");
			return;
		}

		// validacija
		if (ime.equals("")) {
			response.sendRedirect(baseURL + "Zanrovi/Create");
			return;
		}
		if (opis.equals("")) {
			response.sendRedirect(baseURL + "Zanrovi/Create");
			return;
		}

		Zanr zanr = new Zanr(id, ime, opis);
		zanrService.save(zanr);

		response.sendRedirect(baseURL + "Zanrovi");
	}

	@PostMapping(value="/Edit")
	public void edit(@RequestParam Long id, 
			@RequestParam String ime, @RequestParam String opis, 
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Zanrovi");
			return;
		}

		// validacija
		Zanr zanr = zanrService.findOne(id);
		if (zanr == null) {
			response.sendRedirect(baseURL + "Zanrovi");
			return;
		}	
		if (ime == null || ime.equals("")) {
			response.sendRedirect(baseURL + "Zanrovi/Details?id=" + id);
			return;
		}
		
		if (opis == null || opis.equals("")) {
			response.sendRedirect(baseURL + "Zanrovi/Details?id=" + id);
			return;
		}

		// izmena
		zanr.setIme(ime);
		zanr.setOpis(opis);
		zanrService.update(zanr);

		response.sendRedirect(baseURL + "Zanrovi");
	}

	@PostMapping(value="/Delete")
	public void delete(@RequestParam Long id, 
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Zanrovi");
			return;
		}

		zanrService.delete(id);

		response.sendRedirect(baseURL + "Zanrovi");
	}

}
