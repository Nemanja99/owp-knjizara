package EKnjizara.KnjizaraGolub.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import EKnjizara.KnjizaraGolub.model.Knjiga;
import EKnjizara.KnjizaraGolub.model.Korisnik;
import EKnjizara.KnjizaraGolub.service.KnjigaService;

@Controller
@RequestMapping(value="/Knjige")
public class KnjigeController implements ServletContextAware{

	public static final String STATISTIKA_KNJIGA_KEY = "statistikaKnjiga";
	public static final String POSECENE_KNJIGE_ZA_KORISNIKA_KEY = "poseceneKnjigeZaKorisnika";
	
	@Autowired
	private KnjigaService knjigaService;

	/*@Autowired
	private ZanrService zanrService;
	*/
	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) String naziv, 
			@RequestParam(required=false) Integer isbn, 
			@RequestParam(required=false) String izdavackaKuca,
			@RequestParam(required=false) String autor,
			@RequestParam(required=false) Integer godinaIzdanja, 
			@RequestParam(required=false) String opis,
			@RequestParam(required=false) String slika,
			@RequestParam(required=false) Double cena,
			@RequestParam(required=false) Integer brStranica, 
			@RequestParam(required=false) String tipPoveza,
			@RequestParam(required=false) String pismo,
			@RequestParam(required=false) String jezik,
			//@RequestParam(required=false) Double prosOcena,
			HttpSession session)  throws IOException {
		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		
		System.out.println("ispis");
		
		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		if(izdavackaKuca!=null && izdavackaKuca.trim().equals(""))
			izdavackaKuca=null;
		if(autor!=null && autor.trim().equals(""))
			autor=null;
		if(opis!=null && opis.trim().equals(""))
			opis=null;
		if(tipPoveza!=null && tipPoveza.trim().equals(""))
			tipPoveza=null;
		if(pismo!=null && pismo.trim().equals(""))
			pismo=null;
		if(jezik!=null && jezik.trim().equals(""))
			jezik=null;
		
		List<Knjiga> knjige = knjigaService.find(naziv, isbn, izdavackaKuca, autor, godinaIzdanja, opis, tipPoveza, pismo, jezik);
		//List<Zanr> zanrovi = zanrService.findAll();

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("knjige");
		rezultat.addObject("knjige", knjige);
		//rezultat.addObject("zanrovi", zanrovi);

		return rezultat;
	}

	@GetMapping(value="/Details")
	@SuppressWarnings("unchecked")
	public ModelAndView details(@RequestParam Long id, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Knjiga knjiga = knjigaService.findOne(id);
		//List<Zanr> zanrovi = ZanrService.findAll();

		

		

		ModelAndView rezultat = new ModelAndView("knjiga");
		rezultat.addObject("knjiga", knjiga);
		//rezultat.addObject("zanrovi", zanrovi);

		return rezultat;
	}

	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return null;
		}

		//List<Zanr> zanrovi = zanrService.findAll();

		ModelAndView rezultat = new ModelAndView("dodavanjeKnjige");
		//rezultat.addObject("zanrovi", zanrovi);

		return rezultat;
	}

	@PostMapping(value="/Create")
	public void create(@RequestParam Long id, @RequestParam String naziv, @RequestParam int isbn, @RequestParam String izdavackaKuca, @RequestParam String autor, @RequestParam int godinaIzdanja ,@RequestParam String opis, @RequestParam double cena, @RequestParam int brStranica, @RequestParam String tipPoveza, @RequestParam String pismo, @RequestParam String jezik/* @RequestParam double prosecnaOcena @RequestParam(name="zanrId", required=false) Long[] zanrIds*/, 
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}

		Knjiga knjiga = new Knjiga(id, naziv, isbn, izdavackaKuca, autor, godinaIzdanja, opis, cena, brStranica, tipPoveza, pismo, jezik );
		//knjiga.setZanrovi(ZanrService.find(zanrIds));
		knjigaService.save(knjiga);

		response.sendRedirect(baseURL + "Knjige");
	}

	@PostMapping(value="/Edit")
	public void edit(@RequestParam Long id, 
			@RequestParam String naziv,  @RequestParam int isbn, @RequestParam String izdavackaKuca, @RequestParam String autor, @RequestParam int godinaIzdanja ,@RequestParam String opis, @RequestParam double cena, @RequestParam int brStranica, @RequestParam String tipPoveza, @RequestParam String pismo, @RequestParam String jezik,/* @RequestParam double prosecnaOcena, @RequestParam(name="zanrId", required=false) Long[] zanrIds, */
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}

		Knjiga knjiga = knjigaService.findOne(id);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}	
		if (naziv == null || naziv.equals("")) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (isbn == 0) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (izdavackaKuca == null || izdavackaKuca.equals("")) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (autor == null || autor.equals("")) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (godinaIzdanja == 0) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (opis == null || opis.equals("")) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (cena == 0) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (brStranica == 0) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (tipPoveza == null || tipPoveza.equals("")) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (pismo == null || pismo.equals("")) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		if (jezik == null || jezik.equals("")) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}
		/*if (prosecnaOcena == 0) {
			response.sendRedirect(baseURL + "Knjige/Details?id=" + id);
			return;
		}*/

		knjiga.setNaziv(naziv);
		knjiga.setISBN(isbn);
		knjiga.setIzdavackaKuca(izdavackaKuca);
		knjiga.setAutor(autor);
		knjiga.setGodinaIzdanja(godinaIzdanja);
		knjiga.setOpis(opis);
		knjiga.setCena(cena);
		knjiga.setBrStranica(brStranica);
		knjiga.setTipPoveza(tipPoveza);
		knjiga.setPismo(pismo);
		knjiga.setJezik(jezik);
		//knjiga.setProsOcena(prosecnaOcena);
		//knjiga.setZanrovi(zanrService.find(zanrIds));
		knjigaService.update(knjiga);

		response.sendRedirect(baseURL + "Knjige");
	}

	@PostMapping(value="/Delete")
	public void delete(@RequestParam Long id, 
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}

		knjigaService.delete(id);
	
		response.sendRedirect(baseURL + "Knjige");
	}

}
