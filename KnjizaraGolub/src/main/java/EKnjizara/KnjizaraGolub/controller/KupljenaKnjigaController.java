package EKnjizara.KnjizaraGolub.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import EKnjizara.KnjizaraGolub.model.Knjiga;
import EKnjizara.KnjizaraGolub.model.Korisnik;
import EKnjizara.KnjizaraGolub.model.KupljenaKnjiga;
import EKnjizara.KnjizaraGolub.service.KnjigaService;
import EKnjizara.KnjizaraGolub.service.KupljenaKnjigaService;

@Controller
@RequestMapping(value="/KupljenaKnjiga")
public class KupljenaKnjigaController {

public static final String IZABRANE_KNJIGE_ZA_KORISNIKA = "IzabraneKnjigeZaKorisnika";
	
	private  String bURL; 
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private KupljenaKnjigaService kupljenaKnjigaService;
	

	@PostConstruct
	public void init() {	
	
		bURL = servletContext.getContextPath()+"/";			
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping
	public ModelAndView get(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<KupljenaKnjiga> knjige = kupljenaKnjigaService.findAll();

		ModelAndView rezultat = new ModelAndView("knjige");
		rezultat.addObject("knjige", knjige);

		return rezultat;
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping
	public void kupiKnjigu(
			@RequestParam String knjigaId,
			@RequestParam String username,
			@RequestParam double cena,
			@RequestParam String brPrimeraka,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KorisnikController.KORISNIK_KEY);
		if(korisnik==null || korisnik.isAdministrator()==true) {
			response.sendRedirect(bURL+"prijava.html");
			return;
		}
		
		Long idKnjige = new Long(knjigaId);
		
		if(knjigaId!=null && idKnjige<=0) {
			response.sendRedirect(bURL+"Knjige");
			return;
		}
		
		Knjiga knjiga = knjigaService.findOne(idKnjige);
		if(knjiga==null) {
			response.sendRedirect(bURL+"Knjige");
			return;
		}
		
		KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga(knjiga, brPrimeraka, cena);
		kupljenaKnjigaService.save(kupljenaKnjiga);
		
		response.sendRedirect(bURL + "Karte"); 
		return;
	}

}
