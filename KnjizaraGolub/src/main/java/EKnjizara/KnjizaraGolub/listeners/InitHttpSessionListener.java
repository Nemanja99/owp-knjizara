package EKnjizara.KnjizaraGolub.listeners;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;

@Component
public class InitHttpSessionListener  implements HttpSessionListener {
	
	public void sessionCreated(HttpSessionEvent event) {
		System.out.println("Inicijalizacija sesije HttpSessionListener...");

		HttpSession session  = event.getSession();
		System.out.println("Session id korisnika je "+ session.getId());

		

		System.out.println("Uspeh HttpSessionListener!");
	}
	
	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("Brisanje sesije HttpSessionListener...");
		
		System.out.println("Uspeh HttpSessionListener!");
	}

}
