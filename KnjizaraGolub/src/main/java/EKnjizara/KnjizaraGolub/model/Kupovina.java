package EKnjizara.KnjizaraGolub.model;

import java.util.ArrayList;
import java.util.List;

public class Kupovina {

	private int Id;
	private String DatumKupovine;
	private Korisnik Korisnik;
	private double UkupnaCena;
	private String UkupanBrKupljenihKnjiga;
	
	private List<Knjiga> knjige = new ArrayList<>();

	public Kupovina(String datumKupovine,Korisnik korisnik, double ukupnaCena, String ukupanBrKupljenihKnjiga) 
	{
		DatumKupovine = datumKupovine;
		Korisnik = korisnik;
		UkupnaCena = ukupnaCena;
		UkupanBrKupljenihKnjiga = ukupanBrKupljenihKnjiga;
	}
	
	public Kupovina(int id, String datumKupovine, Korisnik korisnik, double ukupnaCena,
			String ukupanBrKupljenihKnjiga) {
		Id = id;
		DatumKupovine = datumKupovine;
		Korisnik = korisnik;
		UkupnaCena = ukupnaCena;
		UkupanBrKupljenihKnjiga = ukupanBrKupljenihKnjiga;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kupovina other = (Kupovina) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getDatumKupovine() {
		return DatumKupovine;
	}

	public void setDatumKupovine(String datumKupovine) {
		DatumKupovine = datumKupovine;
	}

	public Korisnik getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		Korisnik = korisnik;
	}

	public double getUkupnaCena() {
		return UkupnaCena;
	}

	public void setUkupnacena(double ukupnaCena) {
		UkupnaCena = ukupnaCena;
	}

	public String getUkupanBrKupljenihKnjiga() {
		return UkupanBrKupljenihKnjiga;
	}

	public void setUkupanBrKupljenihKnjiga(String ukupanBrKupljenihKnjiga) {
		UkupanBrKupljenihKnjiga = ukupanBrKupljenihKnjiga;
	}

	public List<Knjiga> getKnjige() {
		return knjige;
	}

	public void setKnjige(List<Knjiga> knjige) {
		this.knjige = knjige;
	}

	@Override
	public String toString() {
		return "Kupovina [Id=" + Id + ", DatumKupovine=" + DatumKupovine + ", Korisnik=" + Korisnik + ", Ukupnacena="
				+ UkupnaCena + ", UkupanBrKupljenihKnjiga=" + UkupanBrKupljenihKnjiga + "]";
	}
	
}
