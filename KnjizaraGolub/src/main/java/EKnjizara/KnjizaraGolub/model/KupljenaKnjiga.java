package EKnjizara.KnjizaraGolub.model;

public class KupljenaKnjiga {
	
	private int Id;
	private Knjiga Knjiga;
	private String BrPrimeraka;
	private double cena;
	
	public KupljenaKnjiga( Knjiga knjiga, String brPrimeraka, double cena) {
		Knjiga = knjiga;
		BrPrimeraka = brPrimeraka;
		this.cena = cena;
	}
	
	public KupljenaKnjiga(int id, Knjiga knjiga, String brPrimeraka, double cena) {
		Id = id;
		Knjiga = knjiga;
		BrPrimeraka = brPrimeraka;
		this.cena = cena;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KupljenaKnjiga other = (KupljenaKnjiga) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Knjiga getKnjiga() {
		return Knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		Knjiga = knjiga;
	}

	public String getBrPrimeraka() {
		return BrPrimeraka;
	}

	public void setBrPrimeraka(String brPrimeraka) {
		BrPrimeraka = brPrimeraka;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	@Override
	public String toString() {
		return "KupljenaKnjiga [Id=" + Id + ", Knjiga=" + Knjiga + ", BrPrimeraka=" + BrPrimeraka + ", cena=" + cena
				+ "]";
	}

}
