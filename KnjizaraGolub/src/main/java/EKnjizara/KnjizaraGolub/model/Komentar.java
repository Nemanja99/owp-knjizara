package EKnjizara.KnjizaraGolub.model;

public class Komentar {

	private int Id;
	private String TekstKomentara;
	private int Ocena;
	private String Datum;
	private Korisnik AutorKomentara;
	private Knjiga Knjiga;
	private StatusKomentara Status;
	
	public Komentar(String tekstKomentara, int ocena, String datum, Korisnik autorKomentara,
			Knjiga knjiga, StatusKomentara status) 
	{
		TekstKomentara = tekstKomentara;
		Ocena = ocena;
		Datum = datum;
		AutorKomentara = autorKomentara;
		Knjiga = knjiga;
		Status = status;
	}
	
	public Komentar(int id, String tekstKomentara, int ocena, String datum, Korisnik autorKomentara,
			Knjiga knjiga, StatusKomentara status) {
		Id = id;
		TekstKomentara = tekstKomentara;
		Ocena = ocena;
		Datum = datum;
		AutorKomentara = autorKomentara;
		Knjiga = knjiga;
		Status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Komentar other = (Komentar) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getTekstKomentara() {
		return TekstKomentara;
	}

	public void setTekstKomentara(String tekstKomentara) {
		TekstKomentara = tekstKomentara;
	}

	public int getOcena() {
		return Ocena;
	}

	public void setOcena(int ocena) {
		Ocena = ocena;
	}

	public String getDatum() {
		return Datum;
	}

	public void setDatum(String datum) {
		Datum = datum;
	}

	public Korisnik getAutorKomentara() {
		return AutorKomentara;
	}

	public void setAutorKomentara(Korisnik autorKomentara) {
		AutorKomentara = autorKomentara;
	}

	public Knjiga getKnjiga() {
		return Knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		Knjiga = knjiga;
	}

	public StatusKomentara getStatus() {
		return Status;
	}

	public void setStatus(StatusKomentara status) {
		Status = status;
	}

	@Override
	public String toString() {
		return "Komentar [Id=" + Id + ", TekstKomentara=" + TekstKomentara + ", Ocena=" + Ocena + ", Datum=" + Datum
				+ ", AutorKomentara=" + AutorKomentara + ", Knjiga=" + Knjiga + ", Status=" + Status + "]";
	}
	
}
