package EKnjizara.KnjizaraGolub.model;

public class Zanr {
	
	private Long Id;
	private String Ime;
	private String Opis;
	
	public Zanr(String ime, String opis) {
		Ime = ime;
		Opis = opis;
	}
	
	public Zanr(Long id, String ime, String opis) {
		Id = id;
		Ime = ime;
		Opis = opis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zanr other = (Zanr) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		return true;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getIme() {
		return Ime;
	}

	public void setIme(String ime) {
		Ime = ime;
	}

	public String getOpis() {
		return Opis;
	}

	public void setOpis(String opis) {
		Opis = opis;
	}

	@Override
	public String toString() {
		return "Zanr [Id=" + Id + ", Ime=" + Ime + ", Opis=" + Opis + "]";
	}

}
