package EKnjizara.KnjizaraGolub.model;

public class StatusKomentara {
	
	private int Id;
	private String NaCekanju;
	private String Odobren;
	private String NijeOdobren;
	
	public StatusKomentara(String naCekanju, String odobren, String nijeOdobren) 
	{
		NaCekanju = naCekanju;
		Odobren = odobren;
		NijeOdobren = nijeOdobren;
	}
	
	public StatusKomentara(int id, String naCekanju, String odobren, String nijeOdobren) {
		Id = id;
		NaCekanju = naCekanju;
		Odobren = odobren;
		NijeOdobren = nijeOdobren;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusKomentara other = (StatusKomentara) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getNaCekanju() {
		return NaCekanju;
	}

	public void setNaCekanju(String naCekanju) {
		NaCekanju = naCekanju;
	}

	public String getOdobren() {
		return Odobren;
	}

	public void setOdobren(String odobren) {
		Odobren = odobren;
	}

	public String getNijeOdobren() {
		return NijeOdobren;
	}

	public void setNijeOdobren(String nijeOdobren) {
		NijeOdobren = nijeOdobren;
	}

	@Override
	public String toString() {
		return "StatusKomentara [Id=" + Id + ", NaCekanju=" + NaCekanju + ", Odobren=" + Odobren + ", NijeOdobren="
				+ NijeOdobren + "]";
	}

}
