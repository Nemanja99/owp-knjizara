package EKnjizara.KnjizaraGolub.model;

public class LoyaltyKartica {
	
	private int Id;
	private String Popust;
	private int BrPoena;
	
	public LoyaltyKartica(String popust, int brPoena) {
		Popust = popust;
		BrPoena = brPoena;
	}
	
	public LoyaltyKartica(int id, String popust, int brPoena) {
		Id = id;
		Popust = popust;
		BrPoena = brPoena;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoyaltyKartica other = (LoyaltyKartica) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getPopust() {
		return Popust;
	}

	public void setPopust(String popust) {
		Popust = popust;
	}

	public int getBrPoena() {
		return BrPoena;
	}

	public void setBrPoena(int brPoena) {
		BrPoena = brPoena;
	}

	@Override
	public String toString() {
		return "LoyaltyKartica [Id=" + Id + ", Popust=" + Popust + ", BrPoena=" + BrPoena + "]";
	}

}
