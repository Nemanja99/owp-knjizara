package EKnjizara.KnjizaraGolub.model;

public class Knjiga {
	
	private Long Id;
	private String Naziv;
	private int ISBN;
	private String IzdavackaKuca;
	private String Autor;
	private int GodinaIzdanja;
	private String Opis;
	private String Slika;
	private double Cena;
	private int BrStranica;
	private String TipPoveza;
	private String Pismo;
	private String Jezik;
	private double ProsOcena;
	
	public Knjiga(String naziv ,int iSBN, String izdavackaKuca, String autor, int godinaIzdanja, String opis, /*String slika,*/
			double cena, int brStranica, String tipPoveza, String pismo, String jezik/*, double prosOcena*/) 
	{
		Naziv = naziv;
		ISBN = iSBN;
		IzdavackaKuca = izdavackaKuca;
		Autor = autor;
		GodinaIzdanja = godinaIzdanja;
		Opis = opis;
		//kreirati za sliku
		Cena = cena;
		BrStranica = brStranica;
		TipPoveza = tipPoveza;
		Pismo = pismo;
		Jezik = jezik;
		//ProsOcena = prosOcena;
	}

	public Knjiga(Long id, String naziv, int iSBN, String izdavackaKuca, String autor, int godinaIzdanja, String opis,
			double cena, int brStranica, String tipPoveza, String pismo, String jezik) {
		super();
		Id = id;
		Naziv = naziv;
		ISBN = iSBN;
		IzdavackaKuca = izdavackaKuca;
		Autor = autor;
		GodinaIzdanja = godinaIzdanja;
		Opis = opis;
		Cena = cena;
		BrStranica = brStranica;
		TipPoveza = tipPoveza;
		Pismo = pismo;
		Jezik = jezik;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Id ^ (Id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Knjiga other = (Knjiga) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNaziv() {
		return Naziv;
	}

	public void setNaziv(String naziv) {
		Naziv = naziv;
	}

	public int getISBN() {
		return ISBN;
	}

	public void setISBN(int iSBN) {
		ISBN = iSBN;
	}

	public String getIzdavackaKuca() {
		return IzdavackaKuca;
	}

	public void setIzdavackaKuca(String izdavackaKuca) {
		IzdavackaKuca = izdavackaKuca;
	}

	public String getAutor() {
		return Autor;
	}

	public void setAutor(String autor) {
		Autor = autor;
	}

	public int getGodinaIzdanja() {
		return GodinaIzdanja;
	}

	public void setGodinaIzdanja(int godinaIzdanja) {
		GodinaIzdanja = godinaIzdanja;
	}

	public String getOpis() {
		return Opis;
	}

	public void setOpis(String opis) {
		Opis = opis;
	}

	public String getSlika() {
		return Slika;
	}

	public void setSlika(String slika) {
		Slika = slika;
	}

	public double getCena() {
		return Cena;
	}

	public void setCena(double cena) {
		Cena = cena;
	}

	public int getBrStranica() {
		return BrStranica;
	}

	public void setBrStranica(int brStranica) {
		BrStranica = brStranica;
	}

	public String getTipPoveza() {
		return TipPoveza;
	}

	public void setTipPoveza(String tipPoveza) {
		TipPoveza = tipPoveza;
	}

	public String getPismo() {
		return Pismo;
	}

	public void setPismo(String pismo) {
		Pismo = pismo;
	}

	public String getJezik() {
		return Jezik;
	}

	public void setJezik(String jezik) {
		Jezik = jezik;
	}

	public double getProsOcena() {
		return ProsOcena;
	}

	public void setProsOcena(double prosOcena) {
		ProsOcena = prosOcena;
	}
	
	@Override
	public String toString() {
		return "Knjiga [Id=" + Id + ", Naziv=" + Naziv + ", ISBN=" + ISBN + ", IzdavackaKuca=" + IzdavackaKuca + ", Autor=" + Autor + ", GodinaIzdanja=" + GodinaIzdanja + ", Opis=" + Opis + ",Slika=" + Slika + ", Cena=" + Cena + ", BrStranica= " + BrStranica + ",TipPoveza=" + TipPoveza + ",Pismo=" + Pismo + ",Jezik=" + Jezik +", ProsOcena=" + ProsOcena + "]";
	}

}
