package EKnjizara.KnjizaraGolub.model;

public class Korisnik {
	
	private Long Id;
	private String Username;
	private String Password;
	private String Email;
	private String Ime;
	private String Prezime;
	private String DatumRodjenja;
	private String Adresa;
	private String BrojTelefona;
	private String DatumVremeRegistracije;
	//private String Uloga;
	private boolean administrator = false;
	
	public Korisnik() {}
	
	public Korisnik(String username, String password, String email, String ime, String prezime,
			String datumRodjenja, String adresa, String brojTelefona,/* String datumVremeRegistracije, String uloga*/ boolean administrator) 
	{
		Username = username;
		Password = password;
		Email = email;
		Ime = ime;
		Prezime = prezime;
		DatumRodjenja = datumRodjenja;
		Adresa = adresa;
		BrojTelefona = brojTelefona;
		//DatumVremeRegistracije = datumVremeRegistracije;
		this.administrator = administrator;
		//Uloga = uloga;
	}
	
	public Korisnik(/*int id,*/ String username, String password, String email, String ime, String prezime,
			String datumRodjenja, String adresa, String brojTelefona/*, String datumVremeRegistracije, String uloga*/) {
		//Id = id;
		Username = username;
		Password = password;
		Email = email;
		Ime = ime;
		Prezime = prezime;
		DatumRodjenja = datumRodjenja;
		Adresa = adresa;
		BrojTelefona = brojTelefona;
		//DatumVremeRegistracije = datumVremeRegistracije;
		//Uloga = uloga;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Korisnik other = (Korisnik) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		return true;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getIme() {
		return Ime;
	}

	public void setIme(String ime) {
		Ime = ime;
	}

	public String getPrezime() {
		return Prezime;
	}

	public void setPrezime(String prezime) {
		Prezime = prezime;
	}

	public String getDatumRodjenja() {
		return DatumRodjenja;
	}

	public void setDatumRodjenja(String datumRodjenja) {
		DatumRodjenja = datumRodjenja;
	}

	public String getAdresa() {
		return Adresa;
	}

	public void setAdresa(String adresa) {
		Adresa = adresa;
	}

	public String getBrojTelefona() {
		return BrojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		BrojTelefona = brojTelefona;
	}

	public String getDatumVremeRegistracije() {
		return DatumVremeRegistracije;
	}

	public void setDatumVremeRegistracije(String datumVremeRegistracije) {
		DatumVremeRegistracije = datumVremeRegistracije;
	}
	
	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	/*public String getUloga() {
		return Uloga;
	}

	public void setUloga(String uloga) {
		Uloga = uloga;
	}*/

	@Override
	public String toString() {
		return "Korisnik [Id=" + Id + ", Username=" + Username + ", Password=" + Password + ", Email=" + Email
				+ ", Ime=" + Ime + ", Prezime=" + Prezime + ", DatumRodjenja=" + DatumRodjenja + ", Adresa=" + Adresa
				+ ", BrojTelefona=" + BrojTelefona + ", DatumVremeRegistracije=" + DatumVremeRegistracije + ", administrator=" + administrator + /*", Uloga="
				+ Uloga +*/ "]";
	}
	
}
