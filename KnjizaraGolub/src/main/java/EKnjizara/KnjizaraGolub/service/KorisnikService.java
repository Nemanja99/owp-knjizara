package EKnjizara.KnjizaraGolub.service;

import java.util.List;

import org.springframework.stereotype.Service;

import EKnjizara.KnjizaraGolub.model.Korisnik;

public interface KorisnikService {
	
	Korisnik findOne(String username);
	Korisnik findOne(String username, String password);
	List<Korisnik> findAll();
	Korisnik save(Korisnik korisnik);
	List<Korisnik> save(List<Korisnik> korisnici);
	Korisnik update(Korisnik korisnik);
	List<Korisnik> update(List<Korisnik> korisnici);
	Korisnik delete(String username);
	void delete(List<String> usernames);
	List<Korisnik> find(String username, String eMail, String ime, String prezime, String datumRodjenja, String adresa, String brojTelefona, Boolean administrator);
	List<Korisnik> findByUsername(String username);

}
