package EKnjizara.KnjizaraGolub.service;

import java.util.List;

import EKnjizara.KnjizaraGolub.model.KupljenaKnjiga;

public interface KupljenaKnjigaService {
	
	KupljenaKnjiga findOne(Long id);
	List<KupljenaKnjiga> findAll();
	KupljenaKnjiga save(KupljenaKnjiga kupljenaKnjiga);
	KupljenaKnjiga update(KupljenaKnjiga kupljenaKnjiga);
	KupljenaKnjiga delete(Long id);

}
