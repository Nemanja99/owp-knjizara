package EKnjizara.KnjizaraGolub.service;

import java.util.List;

import org.springframework.stereotype.Service;

import EKnjizara.KnjizaraGolub.model.Knjiga;
import EKnjizara.KnjizaraGolub.model.Zanr;

public interface KnjigaService {
	
	Knjiga findOne(Long id);
	List<Knjiga> findAll();
	Knjiga save(Knjiga knjiga);
	List<Knjiga> save(List<Knjiga> knjige);
	Knjiga update(Knjiga knjiga);
	List<Knjiga> update(List<Knjiga> knjige);
	Knjiga delete(Long id);
	List<Knjiga> deleteAll(Zanr zanr);
	void delete(List<Long> ids);
	List<Knjiga> find(String naziv, Integer isbn, String izdavackaKuca, String autor, Integer godinaIzdanja, String opis, String tipPoveza, String pismo, String jezik);
	List<Knjiga> findByZanrId(Long zanrId);

}
