package EKnjizara.KnjizaraGolub.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import EKnjizara.KnjizaraGolub.dao.KupljenaKnjigaDAO;
import EKnjizara.KnjizaraGolub.model.KupljenaKnjiga;
import EKnjizara.KnjizaraGolub.service.KupljenaKnjigaService;

@Service
public class DatabaseKupljenaKnjigaService implements KupljenaKnjigaService{

	@Autowired
	private KupljenaKnjigaDAO kupljenaKnjigaDAO;
	
	@Override
	public KupljenaKnjiga findOne(Long id) {
		return kupljenaKnjigaDAO.findOne(id);
	}

	@Override
	public List<KupljenaKnjiga> findAll() {
		return kupljenaKnjigaDAO.findAll();
	}

	@Override
	public KupljenaKnjiga save(KupljenaKnjiga kupljenaKnjiga) {
		kupljenaKnjigaDAO.save(kupljenaKnjiga);
		return kupljenaKnjiga;
	}

	@Override
	public KupljenaKnjiga update(KupljenaKnjiga kupljenaKnjiga) {
		kupljenaKnjigaDAO.update(kupljenaKnjiga);
		return kupljenaKnjiga;
	}

	@Override
	public KupljenaKnjiga delete(Long id) {
		KupljenaKnjiga kupljenaKnjiga = findOne(id);
		if (kupljenaKnjiga != null) {
			kupljenaKnjigaDAO.delete(id);
		}
		return kupljenaKnjiga;
	}

}
