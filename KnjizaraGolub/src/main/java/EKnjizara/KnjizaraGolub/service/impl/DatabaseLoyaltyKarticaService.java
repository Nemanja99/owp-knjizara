package EKnjizara.KnjizaraGolub.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import EKnjizara.KnjizaraGolub.dao.LoyaltyKarticaDAO;
import EKnjizara.KnjizaraGolub.model.LoyaltyKartica;
import EKnjizara.KnjizaraGolub.service.LoyaltyKarticaService;

public class DatabaseLoyaltyKarticaService implements LoyaltyKarticaService{

	@Autowired
	private LoyaltyKarticaDAO loyaltyKarticaDAO;
	
	@Override
	public LoyaltyKartica findOne(Long id) {
		return loyaltyKarticaDAO.findOne(id);
	}

	@Override
	public List<LoyaltyKartica> findAll() {
		return loyaltyKarticaDAO.findAll();
	}

	@Override
	public LoyaltyKartica save(LoyaltyKartica loyaltyKartica) {
		loyaltyKarticaDAO.save(loyaltyKartica);
		return loyaltyKartica;
	}

	@Override
	public LoyaltyKartica update(LoyaltyKartica loyaltyKartica) {
		loyaltyKarticaDAO.update(loyaltyKartica);
		return loyaltyKartica;
	}

	@Override
	public LoyaltyKartica delete(Long id) {
		LoyaltyKartica loyaltyKartica = findOne(id);
		if (loyaltyKartica != null) {
			loyaltyKarticaDAO.delete(id);
		}
		return loyaltyKartica;
	}

}

