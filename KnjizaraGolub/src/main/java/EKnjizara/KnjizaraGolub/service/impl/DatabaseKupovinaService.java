package EKnjizara.KnjizaraGolub.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import EKnjizara.KnjizaraGolub.dao.KupovinaDAO;
import EKnjizara.KnjizaraGolub.model.Kupovina;
import EKnjizara.KnjizaraGolub.service.KupovinaService;

@Service
public class DatabaseKupovinaService implements KupovinaService{

	@Autowired
	private KupovinaDAO kupovinaDAO;
	
	@Override
	public Kupovina findOne(Long id) {
		return kupovinaDAO.findOne(id);
	}

	@Override
	public List<Kupovina> findAll() {
		return kupovinaDAO.findAll();
	}

	@Override
	public Kupovina save(Kupovina kupovina) {
		kupovinaDAO.save(kupovina);
		return kupovina;
	}

	@Override
	public Kupovina update(Kupovina kupovina) {
		kupovinaDAO.update(kupovina);
		return kupovina;
	}

	@Override
	public Kupovina delete(Long id) {
		Kupovina kupovina = findOne(id);
		if (kupovina != null) {
			kupovinaDAO.delete(id);
		}
		return kupovina;
	}

}
