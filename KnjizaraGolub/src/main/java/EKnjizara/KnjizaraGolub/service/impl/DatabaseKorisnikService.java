package EKnjizara.KnjizaraGolub.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import EKnjizara.KnjizaraGolub.dao.KorisnikDAO;
import EKnjizara.KnjizaraGolub.model.Korisnik;
import EKnjizara.KnjizaraGolub.service.KorisnikService;


@Service
public class DatabaseKorisnikService implements KorisnikService{

	@Autowired
	private KorisnikDAO korisnikDAO;
	
	@Override
	public Korisnik findOne(String username) {
		return korisnikDAO.findOne(username);
	}

	@Override
	public Korisnik findOne(String username, String password) {
		return korisnikDAO.findOne(username, password);
	}

	@Override
	public List<Korisnik> findAll() {
		return korisnikDAO.findAll();
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		korisnikDAO.save(korisnik);
		return korisnik;
	}

	@Override
	public List<Korisnik> save(List<Korisnik> korisnici) {

		return null;
	}

	@Override
	public Korisnik update(Korisnik korisnik) {
		korisnikDAO.update(korisnik);
		return korisnik;
	}

	@Override
	public List<Korisnik> update(List<Korisnik> korisnici) {

		return null;
	}

	@Override
	public Korisnik delete(String username) {
		Korisnik korisnik = findOne(username);
		if (korisnik != null) {
			korisnikDAO.delete(username);
		}
		return korisnik;
	}

	@Override
	public void delete(List<String> usernames) {

		
	}

	@Override
	public List<Korisnik> find(String username, String eMail, String ime, String prezime, String datumRodjenja, String adresa, String brojTelefona, Boolean administrator) {
		// minimalne inkluzivne vrednosti parametara ako su izostavljeni
		//1. način bi bilo pozivanje ogovarajuće DAO metode u odnosu na broj parametara 
		//		gde bi trebalo implementirati više dao metoda tako da pokriju različite situacije
		//2. način reši sve u DAO sloju
		
		//odabran 2.
		return korisnikDAO.find(username, eMail, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator);
	}

	@Override
	public List<Korisnik> findByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

}