package EKnjizara.KnjizaraGolub.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import EKnjizara.KnjizaraGolub.dao.ZanrDAO;
import EKnjizara.KnjizaraGolub.model.Zanr;
import EKnjizara.KnjizaraGolub.service.ZanrService;

@Service
public class DatabaseZanrService implements ZanrService{
	
	@Autowired
//	@Qualifier("zanrDAOOldCode")
	private ZanrDAO zanrDAO;

	@Override
	public Zanr findOne(Long id) {
		return zanrDAO.findOne(id);
	}

	@Override
	public List<Zanr> findAll() {
		return zanrDAO.findAll();
	}

	@Override
	public List<Zanr> find(Long[] ids) {
		List<Zanr> rezultat = new ArrayList<>();
		for (Long id: ids) {
			Zanr zanr = zanrDAO.findOne(id);
			rezultat.add(zanr);
		}

		return rezultat;
	}

	@Override
	public Zanr save(Zanr zanr) {
		zanrDAO.save(zanr);
		return zanr;
	}

	@Override
	public List<Zanr> save(List<Zanr> zanrovi) {

		return null;
	}

	@Override
	public Zanr update(Zanr zanr) {
		zanrDAO.update(zanr);
		return zanr;
	}

	@Override
	public List<Zanr> update(List<Zanr> zanrovi) {

		return null;
	}

	@Override
	public Zanr delete(Long id) {
		Zanr zanr = findOne(id);
		if (zanr != null) {
			zanrDAO.delete(id);
		}
		return zanr;
	}

	@Override
	public void delete(List<Long> ids) 
	{
		
	}

	@Override
	public List<Zanr> find(String ime, String opis) {
		// minimalne inkluzivne vrednosti parametara ako su izostavljeni
		//1. način bi bilo pozivanje ogovarajuće DAO metode u odnosu na broj parametara 
		//		gde bi trebalo implementirati više dao metoda tako da pokriju različite situacije
		//2. način reši sve u DAO sloju
		
		List<Zanr> zanrovi = zanrDAO.findAll();
		
		//odabran 1.
		if (ime == null) {
			ime = "";
		}
		
		if (opis == null) {
			opis = "";
		}
		
		List<Zanr> rezultat = new ArrayList<>();
		for(Zanr itZanr: zanrovi)
		{
			if (!itZanr.getIme().toLowerCase().contains(ime.toLowerCase())) {
				continue;
			}
			
			if (!itZanr.getOpis().toLowerCase().contains(opis.toLowerCase())) {
				continue;
			}
			
			rezultat.add(itZanr);
		}
		
		return rezultat;
	}

}