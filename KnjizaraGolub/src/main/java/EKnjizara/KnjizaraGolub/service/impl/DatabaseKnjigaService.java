package EKnjizara.KnjizaraGolub.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import EKnjizara.KnjizaraGolub.dao.KnjigaDAO;
import EKnjizara.KnjizaraGolub.model.Knjiga;
import EKnjizara.KnjizaraGolub.model.Zanr;
import EKnjizara.KnjizaraGolub.service.KnjigaService;


@Service
public class DatabaseKnjigaService  implements KnjigaService{

	@Autowired
	private KnjigaDAO knjigaDAO;
	
	@Override
	public Knjiga findOne(Long id) {
		return knjigaDAO.findOne(id);
	}

	@Override
	public List<Knjiga> findAll() {
		return knjigaDAO.findAll();
	}

	@Override
	public Knjiga save(Knjiga knjiga) {
		knjigaDAO.save(knjiga);
		return knjiga;
	}

	@Override
	public List<Knjiga> save(List<Knjiga> knjige) {

		return null;
	}

	@Override
	public Knjiga update(Knjiga knjiga) {
		knjigaDAO.update(knjiga);
		return knjiga;
	}

	@Override
	public List<Knjiga> update(List<Knjiga> knjige) {

		return null;
	}

	@Override
	public Knjiga delete(Long id) {
		Knjiga knjiga = findOne(id);
		if (knjiga != null)
			knjigaDAO.delete(id);
		
		return knjiga;
	}

	@Override
	public List<Knjiga> deleteAll(Zanr zanr) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(List<Long> ids) {
		// TODO Auto-generated method stub
	}
	//NACIN 1
	//U ovom metodi pretragu radimo manuelno tako što programski filtriramo kompletnu listu rezultata dobijenu sa findAll()
	//očitaju se svi filmovi iz baze pa se filtrira po uslovu
	//nije praktično imajući u vidu da u bazi može biti i 500.000 fimova
	//NACIN 2
	//ideja je koristiti select sa where delom sa se smanji ResultSet
	@Override
	public List<Knjiga> find(String naziv, Integer isbn, String izdavackaKuca, String autor, Integer godinaIzdanja, String opis, String tipPoveza, String pismo, String jezik) {
		//NACIN 1
		List<Knjiga> knjige = knjigaDAO.findAll();

		// maksimalno inkluzivne vrednosti parametara ako su izostavljeni
		// filtiranje radi u Servisnom sloju - izbegavati
		if (naziv == null) {
			naziv = "";
		}
		if (isbn == null) {
			isbn = 0;
		}
		if (izdavackaKuca == null) {
			izdavackaKuca = "";
		}
		if (autor == null) {
			autor = "";
		}
		if (godinaIzdanja == null) {
			godinaIzdanja = 0;
		}
		if (opis == null) {
			opis = "";
		}
		if (tipPoveza == null) {
			tipPoveza = "";
		}
		if (pismo == null) {
			pismo = "";
		}
		if (jezik == null) {
			jezik = "";
		}
		
		List<Knjiga> rezultat = new ArrayList<>();
		for (Knjiga itKnjiga: knjige) {
			// kriterijum pretrage
			if (!itKnjiga.getNaziv().toLowerCase().contains(naziv.toLowerCase())) {
				continue;
			}
			
			if (!itKnjiga.getAutor().toLowerCase().contains(autor.toLowerCase())) {
				continue;
			}
			
			if (!itKnjiga.getIzdavackaKuca().toLowerCase().contains(izdavackaKuca.toLowerCase())) {
				continue;
			}
			
			if (!itKnjiga.getOpis().toLowerCase().contains(opis.toLowerCase())) {
				continue;
			}
			
			if (!itKnjiga.getTipPoveza().toLowerCase().contains(tipPoveza.toLowerCase())) {
				continue;
			}
			
			if (!itKnjiga.getPismo().toLowerCase().contains(pismo.toLowerCase())) {
				continue;
			}
			
			if (!itKnjiga.getJezik().toLowerCase().contains(jezik.toLowerCase())) {
				continue;
			}

			rezultat.add(itKnjiga);
		}

		return rezultat;
		
//		//NACIN 2. ideja je koristiti select sa where delom sa se smanji ResultSet
//		return filmDAO.find(naziv, zanrId, trajanjeOd, trajanjeDo);
	}

	@Override
	public List<Knjiga> findByZanrId(Long zanrId) {
		// TODO Auto-generated method stub
		return null;
	}

}