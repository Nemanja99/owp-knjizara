package EKnjizara.KnjizaraGolub.service;

import java.util.List;

import EKnjizara.KnjizaraGolub.model.Kupovina;

public interface KupovinaService {
	
	Kupovina findOne(Long id);
	List<Kupovina> findAll();
	Kupovina save(Kupovina Kupovina);
	Kupovina update(Kupovina Kupovina);
	Kupovina delete(Long id);

}
