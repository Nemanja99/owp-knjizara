package EKnjizara.KnjizaraGolub.service;

import java.util.List;

import EKnjizara.KnjizaraGolub.model.LoyaltyKartica;

public interface LoyaltyKarticaService {

	LoyaltyKartica findOne(Long id);
	List<LoyaltyKartica> findAll();
	LoyaltyKartica save(LoyaltyKartica loyaltyKartica);
	LoyaltyKartica update(LoyaltyKartica loyaltyKartica);
	LoyaltyKartica delete(Long id);
	
}
